#include "image.h"
#include "bmp_functions.h"
#include "util.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    if(argc != 3){
        fprintf(stderr, "The number of arguments must be equal to 2");
        return -1;
    }

    FILE *in, *out;

    if (open_file(&in, argv[1], "rb") && open_file(&out, argv[2], "wb")){
        struct image img;

        enum read_status readStatus = from_bmp(in, &img);
        if(!close_file(&in)){
            fprintf(stderr, "%s\n", "Error occurred while closing input file");
            close_file(&out);
            return -1;
        }

        switch (readStatus) {
            case READ_OK:
                break;
            case READ_UNEXPECTED_EOF:
                fprintf(stderr, "%s\n", "Unexpected EOF");
                return -1;
            case READ_INVALID_BITS:
                return -1;
            case READ_ERROR:
                fprintf(stderr, "%s\n", "Error occurred while reading");
                return -1;
            default:
                fprintf(stderr, "%s\n", "Unhandled value of read_status enum");
                return -1;
        }

        struct image rot = rotate(&img);
        free_image(&img);

        enum write_status writeStatus = to_bmp(out, &rot);

        switch (writeStatus) {
            case WRITE_ERROR:
                fprintf(stderr, "%s\n", "Error occurred while writing image");
                return -1;
            case HEADER_WRITE_ERROR:
                fprintf(stderr, "%s\n", "Header writing error");
                return -1;
            case WRITE_OK:
                break;
            default:
                fprintf(stderr, "%s\n", "Unhandled value of write_status enum");
                return -1;
        }

        free_image(&rot);
        if(!close_file(&out)){
            fprintf(stderr, "%s\n", "Error occurred while closing output file");
            return -1;
        }
    } else{
        fprintf(stderr, "%s\n", "Unable to open listed files");
        return -1;
    }

    return 0;
}
