#include "bmp.h"
#include "bmp_functions.h"
#include "image.h"
#include "stdbool.h"
#include "stdio.h"

#define BM 0x4D42
#define RESERVED 0
#define OFFSET 54
#define HEADER_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define SIZE_IMAGE 0
#define PPM 2834
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

static uint8_t calc_padding(uint32_t width) {
    return 4 - (3 * width) % 4;
}

static void prepare_header(struct bmp_header* header, struct image const* img){
    header->bfType = BM;
    header->bfReserved = RESERVED;
    header->bOffBits = OFFSET;
    header->biSize = HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biSizeImage = SIZE_IMAGE;
    header->biXPelsPerMeter = PPM;
    header->biYPelsPerMeter = PPM;
    header->biClrUsed = COLORS_USED;
    header->biClrImportant = COLORS_IMPORTANT;

    header->bfileSize = sizeof (struct pixel) * img->width * img->height + sizeof (struct bmp_header);
}

enum read_status from_bmp( FILE* in, struct image* img){
    if(!in || !img) return READ_ERROR;

    struct bmp_header img_bmp_header = {0};
    size_t read_header_ret_code = fread(&img_bmp_header, sizeof (struct bmp_header), 1, in);

    if (read_header_ret_code){
        *img = create_image(img_bmp_header.biWidth, img_bmp_header.biHeight);
        uint8_t padding = calc_padding(img->width);

        for (size_t y = 0; y < img->height; y++){
            size_t ret = fread(img->data + img->width * y, sizeof (struct pixel), img->width, in);
            if (ret != img->width){
                if (feof(in)){
                    return READ_UNEXPECTED_EOF;
                }
                return READ_ERROR;
            }
            if (fseek(in, padding, SEEK_CUR)){
                return READ_ERROR;
            }
        }

        return READ_OK;

    } else {
        if (feof(in)){
            return READ_UNEXPECTED_EOF;
        }
        return READ_INVALID_BITS;
    }
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    if (!out || !img) return WRITE_ERROR;

    struct bmp_header img_bmp_header = {0};
    prepare_header(&img_bmp_header, img);

    if (fwrite(&img_bmp_header, sizeof (struct bmp_header), 1, out) != 1){
        return HEADER_WRITE_ERROR;
    }

    uint8_t padding = calc_padding(img->width);
    struct pixel* garbage = img->data;

    for (size_t y = 0; y < img->height; y++){
        if (fwrite(img->data + (y * img->width), sizeof (struct pixel), img->width, out) != img->width){
            return WRITE_ERROR;
        }
        if (padding != 0 && fwrite(garbage, padding, 1, out) != 1){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
