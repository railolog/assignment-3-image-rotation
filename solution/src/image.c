#include "image.h"
#include "inttypes.h"
#include "stdbool.h"
#include "stdlib.h"

struct image create_image(uint32_t width, uint32_t height) {
    struct image img = {
            .data =  malloc(sizeof(struct pixel) * width * height),
            .width = width,
            .height = height
    };

    return img;
}


void set_pixel(struct image* img, const struct pixel px, uint32_t x, uint32_t y){
    if (y < img->height && x < img->width) { img->data[y * img->width + x] = px; }
}

struct pixel get_pixel(struct image const* img, uint32_t x, uint32_t y){
    return img->data[y * img->width + x];
}

struct image rotate(struct image const* src){
    struct image dest = create_image(src->height, src->width);

    for (size_t i = 0; i < dest.height; i++){
        for (size_t j = 0; j < dest.width; j++){
            set_pixel(&dest, get_pixel(src, i, src->height - 1 - j), j, i);
        }
    }

    return dest;
}

void free_image(struct image* img){
    free(img->data);
    img->data = NULL;
}
