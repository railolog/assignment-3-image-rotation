#include "util.h"

bool open_file(FILE** file, const char* fname, const char* mode){
    *file = fopen(fname, mode);

    if (!*file){
        return false;
    }
    return true;
}

bool close_file(FILE** file){
    if (fclose(*file) == 0){
        return true;
    }
    return false;
}
