//
// Created by railo on 18.11.2022.
//

#ifndef IMAGE_H
#define IMAGE_H

#include "inttypes.h"
#include "stdbool.h"

#pragma pack(push, 1)
struct pixel {uint8_t b, g, r;};
#pragma pack(pop)

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image create_image(uint32_t width, uint32_t height);
struct image rotate(struct image const* src);
void free_image(struct image* img);
#endif
