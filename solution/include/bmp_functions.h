#ifndef IMAGE_TRANSFORMER_BMP_FUNCTIONS_H
#define IMAGE_TRANSFORMER_BMP_FUNCTIONS_H

#include "image.h"
#include "stdio.h"

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_UNEXPECTED_EOF,
    READ_ERROR,
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    HEADER_WRITE_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img);
enum write_status to_bmp( FILE* out, struct image const* img );

#endif //IMAGE_TRANSFORMER_BMP_FUNCTIONS_H
